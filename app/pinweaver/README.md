# PinWeaver Trusty App

The Trusty app that accesses [PinWeaver] on the Google Security Chip.
Other apps use `trusty/user/desktop/interface/pinweaver` to access the service exposing an API.

[PinWeaver]: https://chromium.googlesource.com/chromiumos/platform/pinweaver/+/refs/heads/main
