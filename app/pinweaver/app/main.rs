fn main() {
    pinweaver::init_and_start_loop().expect("PinWeaver service shouldn't terminate");
}
