# PinWeaver Trusty Interface

This package defines the interface for other Trusty apps to access [PinWeaver].

`storage` defines the interface for the PinWeaver app to communicate with its
storage daemon running in Android.

[PinWeaver]: https://chromium.googlesource.com/chromiumos/platform/pinweaver/+/refs/heads/main
